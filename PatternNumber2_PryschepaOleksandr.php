<?php
function print_pattern($num)
{
  for ($i = 0; $i < $num; $i++){
    for($k = $num; $k > $i+1; $k-- ){
      echo " ";
    }
    for($j = 0; $j <= $i; $j++ ){
      echo "* ";
    }
    echo "\n";
  }
}
$num = 5;
print_pattern($num);
?>